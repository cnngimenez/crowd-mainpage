# Main Page for crowd

This is the main page for crowd. It also contains my personal links and space.

# Objective

Give some links to the wiki and some basic URLs.

# URL

http://crowd.fi.uncoma.edu.ar

# Images

![](https://www.gnu.org/software/guile/static/base/img/kid-programming-a-robot.png) "Kid programming a robot" From Guile web page: https://www.gnu.org/software/guile/

![](https://openclipart.org/image/2400px/svg_to_png/190653/programmingasphere.png) "Programming Sphere" by j4p4n from OpenClipart: https://openclipart.org/detail/190653/programming-sphere
![](https://openclipart.org/image/2400px/svg_to_png/46141/Group-of-People-01.png) "Group of people" by palomaironique from OpenClipart: https://openclipart.org/detail/46141/group-of-people

![](https://www.gnu.org/graphics/meditate-tiny.jpg) "GNU Meditating" from https://www.gnu.org/graphics/meditate.html
![](https://www.gnu.org/graphics/gnu-inside.png) "GNU Inside" from https://www.gnu.org/graphics/gnu-inside.html
![](https://www.gnu.org/graphics/gplv3-127x51.png) "GNU V3 License Logo" from https://www.gnu.org/graphics/license-logos.html
